<?php

namespace Drupal\activity\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * List activities form.
 */
class ActivityListForm extends FormBase {

  /**
   * The connection to the database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The RequestStack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The page manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * Constructs an object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The connection to the database.
   * @param \Symfony\Component\HttpFoundation\RequestStack $stack
   *   The request service.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pagerManager
   *   The page manager.
   */
  public function __construct(Connection $database, RequestStack $stack, PagerManagerInterface $pagerManager) {
    $this->database = $database;
    $this->requestStack = $stack;
    $this->pagerManager = $pagerManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('request_stack'),
      $container->get('pager.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'list_activities_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $results = [];
    $query = $this->database->select('activity_events', 'act');
    $query->fields('act', ['hook', 'label', 'event_id']);
    $results = $query->execute();
    $countResults = $query->countQuery()->execute()->fetchField();
    if ($countResults > 0) {
      $rows = [];
      // Create pagination.
      // Element per page.
      $limit = 15;
      // Current page.
      $current_page = $this->requestStack->getCurrentRequest()->request->get('page');
      if (!empty($current_page)) {
        $page = $current_page;
        $start = $page * $limit;
        $end = ($page + 1) * $limit;
      }
      else {
        $start = 0;
        $end = $limit;
      }
      foreach ($results as $key => $value) {
        if (($key >= $start) && ($key < $end)) {
          $row['label'] = $value->label;
          $row['hook'] = $value->hook;
          $configure_link = Link::fromTextAndUrl($this->t('configure'), URL::fromUri('internal:/admin/activity/configure/' . $value->event_id))
            ->toString();
          $delete_link = Link::fromTextAndUrl($this->t('delete'), URL::fromUri('internal:/admin/activity/delete/' . $value->event_id))
            ->toString();
          $mainLink = $this->t('@configureLink | @deleteLink', [
            '@configureLink' => $configure_link,
            '@deleteLink' => $delete_link,
          ]);
          $row['operations'] = $mainLink;
          $rows[] = $row;
        }
      }
      // Initialize pager.
      $this->pagerManager->createPager($countResults, $limit);
      $form['activity_table'] = [
        '#type' => 'table',
        '#header' => [
          $this->t('LABEL'),
          $this->t('HOOK'),
          $this->t('Operations'),
        ],
        '#attributes' => [
          'id' => 'activity_table',
          'class' => ['activity_table'],
        ],
        '#rows' => $rows,
      ];
      $form['pager'] = ['#type' => 'pager'];
    }
    else {
      $form['no_activities'] = [
        '#type' => 'markup',
        '#markup' => 'There are no Activity Templates created yet. ' . Link::fromTextAndUrl($this->t('Create one now.'), URL::fromUri('internal:/admin/activity/create'))->toString(),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    unset($form['table']['#rows']);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

}
