<?php

namespace Drupal\activity\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Create activities form.
 */
class CreateActivityForm extends MultiStepFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'create_activities_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Events - when to trigger actions.
    $options = [
      'comment_insert' => $this->t('Save new comment'),
      'comment_update' => $this->t('Update comment'),
      'comment_delete' => $this->t('Delete comment'),
      'node_insert' => $this->t('Save new node'),
      'node_update' => $this->t('Update node'),
      'node_delete' => $this->t('Delete node'),
      'user_insert' => $this->t('Save new user'),
      'user_update' => $this->t('Update user'),
      'user_delete' => $this->t('Delete user'),
    ];
    $form = parent::buildForm($form, $form_state);

    // Event name.
    $form['activity_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => '',
      '#required' => TRUE,
      '#size' => 30,
      '#attributes' => [
        'class' => [
          'activity_label',
        ],
      ],
    ];

    $form['activity_actions'] = [
      '#type' => 'radios',
      '#title' => $this->t('Choose your hook'),
      '#required' => TRUE,
      '#default_value' => 1,
      '#options' => $options,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $hook = $form_state->getValue('activity_actions');
    $label = $form_state->getValue('activity_label');
    $this->store->set('hook', $hook);
    $this->store->set('label', $label);
    // Move to next form to configure the event.
    $url = Url::fromUri('internal:/admin/activity/configure/new');
    $form_state->setRedirectUrl($url);
  }

}
