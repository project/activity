<?php

namespace Drupal\activity;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;

/**
 * Provides queries for activity.
 */
class QueryActivity {

  /**
   * The connection to the database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal token service container.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The datetime.time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $timeService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a QueryActivity object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The connection to the database.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   A current user instance.
   * @param \Drupal\Core\Utility\Token $token
   *   Drupal token service container.
   * @param \Drupal\Component\Datetime\TimeInterface $time_service
   *   The datetime.time service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    Connection $database,
    AccountProxyInterface $currentUser,
    Token $token,
    TimeInterface $time_service,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    $this->database = $database;
    $this->currentUser = $currentUser;
    $this->token = $token;
    $this->timeService = $time_service;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get row for the field needed.
   *
   * @param string $eventId
   *   The action name.
   * @param string $field
   *   The field needed.
   */
  public function getActivityEventField($eventId, $field) {
    $query = $this->database->select('activity_events', 'act')
      ->fields('act', [$field])
      ->condition('event_id', $eventId)->execute()->fetchAll();
    return $query;
  }

  /**
   * Count all from activity table.
   */
  public function countMessages() {
    $query = $this->database->select('activity', 'act');
    $query->fields('act', ['message']);
    return $query;
  }

  /**
   * Get all activities.
   */
  public function getActivities() {
    $query = $this->database->select('activity', 'activity');
    $query->fields('activity', [
      'action_id',
      'event_id',
      'entity_type',
      'nid',
      'uid',
      'created',
      'status',
      'message',
    ]);
    return $query;
  }

  /**
   * Insert into activity.
   *
   * @param string $eventId
   *   The action name.
   * @param string $entityType
   *   The entity type.
   * @param string $nid
   *   The node id.
   * @param string $uid
   *   The user id.
   * @param string $status
   *   The action status.
   * @param string $message
   *   The message that keeps options from configure activity page.
   */
  public function insertActivity($eventId, $entityType, $nid, $uid, $status, $message) {
    $window = $this->validActivity($eventId);
    // Insert only if the current timestamp - activity Window is greater than 0.
    if ($window) {
      $this->database->insert('activity')
        ->fields([
          'event_id' => $eventId,
          'entity_type' => $entityType,
          'nid' => $nid,
          'uid' => $uid,
          'created' => $this->timeService->getCurrentTime(),
          'status' => $status,
          'message' => $message,
        ])
        ->execute();
    }
  }

  /**
   * Valid function.
   *
   * @param string $eventId
   *   The action id.
   *
   * @return bool
   *   Return the entity.
   */
  public function validActivity($eventId) {
    $timestamp = $this->timeService->getCurrentTime();
    // Get the right value for window option.
    // When to insert logs based on timestamp.
    $queryEvent = $this->database->select('activity_events', 'ev')
      ->fields('ev', ['message'])
      ->condition('event_id', $eventId, '=')
      ->execute()->fetchAll();
    $message = $queryEvent[0]->message;
    $message = json_decode($message);
    $window = $message->window;

    // Count if there are similar actions with the same label
    // in the last $window seconds.
    $count = $this->database->select('activity', 'act')
      ->fields('act', ['action_id'])
      ->condition('event_id', $eventId, '=')
      ->condition('created', $timestamp - $window, '>')
      ->countQuery()
      ->execute()
      ->fetchField();
    return $count == 0;
  }

  /**
   * Get message of specific hook.
   *
   * @param string $hook
   *   Event when the log should be inserted.
   */
  public function getMessage($hook) {
    $query = $this->database->select('activity_events', 'act')
      ->fields('act', ['message', 'event_id'])
      ->condition('hook', $hook)->execute()->fetchAll();
    return $query;
  }

  /**
   * Log actions in table activity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $hook
   *   Event when the log should be inserted.
   */
  public function logActivity(EntityInterface $entity, $hook) {
    $entityNode = NULL;
    $entityUser = NULL;
    $entityComment = NULL;
    $entityType = $entity->getEntityTypeId();
    switch ($entity->getEntityTypeId()) {
      case 'node':
        $activityOption[] = $entity->getType();
        $nodeId = $entity->get('nid')->getValue();
        $nid = $nodeId[0]['value'];
        $userId = $entity->get('uid')->getValue();
        $uid = $userId[0]['target_id'];
        $entityNode = $entity;
        $entityUser = $this->entityTypeManager->getStorage('user')->load($uid);
        $status = $entity->get('status')->getValue();
        break;

      case 'comment':
        $nodeId = $entity->get('entity_id')->getValue();
        $nid = $nodeId[0]['target_id'];
        $entityNode = $this->entityTypeManager->getStorage('node')->load($nid);
        $activityOption[] = $entityNode->getType();
        $entityComment = $entity;
        $userId = $entity->get('uid')->getValue();
        $uid = $userId[0]['target_id'];
        $entityUser = $this->entityTypeManager->getStorage('user')->load($uid);
        $status = $entity->get('status')->getValue();
        break;

      case 'user':
        $rolesOptions = $entity->get('roles');
        $activityOption = ['0' => 'authenticated'];
        foreach ($rolesOptions as $value) {
          $role = $value->getValue();
          $activityOption[] = $role['target_id'];
        }
        $uid = $this->currentUser->id();
        $nid = NULL;
        $entityUser = $entity;
        $status = $entity->get('status')->getValue();
        if ($status[0]['value'] == FALSE) {
          $status[0]['value'] = 0;
        }
        elseif ($status[0]['value'] == TRUE) {
          $status[0]['value'] = 1;
        }
        break;

      default:
    }
    // Insert into activity table all actions.
    $results = $this->getMessage($hook);
    if (!empty($results)) {
      foreach ($results as $value) {
        $message = json_decode($value->message);
        $types = $message->types;
        if (empty($types)) {
          $roles = $message->roles;
        }
        else {
          $types = $message->types;
        }

        if (!empty($activityOption)) {
          if (!empty($types)) {
            foreach ($activityOption as $activityValue) {
              if (in_array($activityValue, $types)) {
                $activityMessage = $this->token->replace($message->message, [
                  'node' => $entityNode,
                  'user' => $entityUser,
                  'comment' => $entityComment,
                ]);
                $this->insertActivity($value->event_id, $entityType, $nid, $uid, $status[0]['value'], $activityMessage);
              }
            }
          }
          elseif (!empty($roles)) {
            if (array_intersect($activityOption, $roles)) {
              $activityMessage = $this->token->replace($message->message, [
                'node' => $entityNode,
                'user' => $entityUser,
                'comment' => $entityComment,
              ]);
              $this->insertActivity($value->event_id, $entityType, $nid, $uid, $status[0]['value'], $activityMessage);
            }
          }
        }
      }
    }
  }

}
